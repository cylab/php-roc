<?php


namespace Cylab\ROC;

/**
 * Class that computes all basic statistics elements.
 * Used to compute ROC and P-R curves.
 * Class Statistics
 * @package Cylab\ROC
 */
class Statistics
{

    private $number_of_elements;

    private int $true_positive_counter = 0;
    private int $true_negative_counter = 0;
    private int $false_positive_counter = 0;
    private int $false_negative_counter = 0;

    private float $true_positive_rate ;
    private float $true_negative_rate;
    private float $false_positive_rate;
    private float $false_negative_rate;

    private float $precision;
    private float $negative_prediction_value;
    private float $false_discovery_rate;
    private float $false_omission_rate;
    private float $accuracy;
    private float $f_1_score;

    /**
     * Statistics constructor.
     * Count the number of elements to generate the confusion matrix.
     * Compute directly Precision because it is needed for P-R curve computation.
     * @param array $points
     * @param float $threshold
     */
    public function __construct(array $points, float $threshold)
    {
        $this->number_of_elements = count($points);

        for ($i = 0; $i < $this->number_of_elements; $i++) {
            if ($points[$i]->isTrueAlert() && $points[$i]->getScore() > $threshold) {
                $this->true_positive_counter++;
            } elseif ($points[$i]->isTrueAlert() && $points[$i]->getScore() < $threshold) {
                $this->false_negative_counter++;
            } elseif ($points[$i]->isFalseAlarm() && $points[$i]->getScore() < $threshold) {
                $this->true_negative_counter++;
            } elseif ($points[$i]->isFalseAlarm() && $points[$i]->getScore() > $threshold) {
                $this->false_positive_counter++;
            }
        }
        $this->computePrecision();
    }

    public function computePrecision()
    {
        if ($this->true_positive_counter + $this->false_positive_counter == 0) {
            $this->precision = NAN;
        } else {
            $this->precision = $this->true_positive_counter /
                ($this->true_positive_counter + $this->false_positive_counter);
        }
    }

    public function computeTruePositiveRate()
    {
        $positive_condition = $this->true_positive_counter
            + $this->false_negative_counter;
        $this->true_positive_rate = $this->true_positive_counter / $positive_condition;
    }

    public function computeTrueNegativeRate()
    {
        $negative_condition = $this->true_negative_counter
            + $this->false_positive_counter;
        $this->true_negative_rate = $this->true_negative_counter / $negative_condition;
    }

    public function computeFalseNegativeRate()
    {
        $tmp = $this->false_negative_counter + $this->true_positive_counter;
        $this->false_negative_rate = $this->false_negative_counter / $tmp;
    }

    public function computeFalsePositiveRate()
    {
        $tmp = $this->false_positive_counter + $this->true_negative_counter;
        $this->false_positive_rate = $this->false_positive_counter / $tmp;
    }

    public function computeNegativePredictionValue()
    {
        $this->negative_prediction_value = $this->true_negative_counter /
            ($this->true_negative_counter + $this->false_negative_counter);
    }

    public function computeFalseDiscoveryRate()
    {
        $this->false_discovery_rate = $this->false_positive_counter /
            ($this->false_positive_counter + $this->true_positive_counter);
    }

    public function computeFalseOmissionRate()
    {
        $this->false_omission_rate = $this->false_negative_counter /
            ($this->false_negative_counter + $this->true_negative_counter);
    }

    public function computeAccuracy()
    {
        $this->accuracy = ($this->true_positive_counter + $this->true_negative_counter) /
            $this->number_of_elements;
    }

    public function computeF1Score()
    {
        $this->f_1_score = (2 * $this->true_positive_counter) /
            (2 * $this->true_positive_counter + $this->false_positive_counter
            + $this->false_negative_counter);
    }

    /**
     * @return mixed
     */
    public function getTruePositiveCounter()
    {
        return $this->true_positive_counter;
    }

    /**
     * @return mixed
     */
    public function getTrueNegativeCounter()
    {
        return $this->true_negative_counter;
    }

    /**
     * @return mixed
     */
    public function getFalsePositiveCounter()
    {
        return $this->false_positive_counter;
    }

    /**
     * @return mixed
     */
    public function getFalseNegativeCounter()
    {
        return $this->false_negative_counter;
    }

    /**
     * @return mixed
     */
    public function getTruePositiveRate()
    {
        $this->computeTruePositiveRate();
        return $this->true_positive_rate;
    }

    public function getRecall()
    {
        $this->computeTruePositiveRate();
        return $this->true_positive_rate;
    }

    /**
     * @return mixed
     */
    public function getTrueNegativeRate()
    {
        $this->computeTrueNegativeRate();
        return $this->true_negative_rate;
    }

    /**
     * @return mixed
     */
    public function getFalsePositiveRate()
    {
        $this->computeFalsePositiveRate();
        return $this->false_positive_rate;
    }

    /**
     * @return mixed
     */
    public function getFalseNegativeRate()
    {
        $this->computeFalseNegativeRate();
        return $this->false_negative_rate;
    }

    /**
     * @return mixed
     */
    public function getPrecision()
    {
        return $this->precision;
    }

    public function setPrecision($precision)
    {
        if ($precision < 0.0 || $precision > 1.0) {
            throw new \Exception('Precesion must be between 0 and 1');
        }
        $this->precision = $precision;
    }

    /**
     * @return mixed
     */
    public function getNegativePredictionValue()
    {
        $this->computeNegativePredictionValue();
        return $this->negative_prediction_value;
    }

    /**
     * @return mixed
     */
    public function getFalseDiscoveryRate()
    {
        $this->computeFalseDiscoveryRate();
        return $this->false_discovery_rate;
    }

    /**
     * @return mixed
     */
    public function getFalseOmissionRate()
    {
        $this->computeFalseOmissionRate();
        return $this->false_omission_rate;
    }

    /**
     * @return mixed
     */
    public function getAccuracy()
    {
        $this->computeAccuracy();
        return $this->accuracy;
    }

    /**
     * @return mixed
     */
    public function getF1Score()
    {
        $this->computeF1Score();
        return $this->f_1_score;
    }
}
