<?php

namespace Cylab\ROC;

use PHPUnit\Framework\TestCase;

class StatisticsTest extends TestCase
{

    private $points = [];
    public function setUp()
    {
        $file = fopen(__DIR__ . "/../resources/RocSampleData.csv", "r");
        while (!feof($file)) {
            $line = fgetcsv($file);
            if (is_numeric($line[0]) && is_numeric($line[1])) {
                $this->points[] = new SimpleValue(floatval($line[0]), boolval($line[1]));
            }
        }
    }

    public function testStatistics1()
    {
        $stats = new Statistics($this->points, 0.5);
        self::assertEquals(6, $stats->getTruePositiveCounter());
        self::assertEquals(4, $stats->getFalsePositiveCounter());
        self::assertEquals(6, $stats->getTrueNegativeCounter());
        self::assertEquals(4, $stats->getFalseNegativeCounter());

        self::assertEquals(0.6, $stats->getTruePositiveRate());
        self::assertEquals(0.4, $stats->getFalsePositiveRate());
        self::assertEquals(0.6, $stats->getTrueNegativeRate());
        self::assertEquals(0.4, $stats->getFalseNegativeRate());

        self::assertEquals(0.6, $stats->getPrecision());
        self::assertEquals(0.6, $stats->getNegativePredictionValue());
        self::assertEquals(0.4, $stats->getFalseDiscoveryRate());
        self::assertEquals(0.4, $stats->getFalseOmissionRate());
        self::assertEquals(0.6, $stats->getAccuracy());
        self::assertEquals(0.6, $stats->getF1Score());
    }

    public function testStatistics2()
    {
        $stats = new Statistics($this->points, 0.2);
        self::assertEquals(10, $stats->getTruePositiveCounter());
        self::assertEquals(9, $stats->getFalsePositiveCounter());
        self::assertEquals(1, $stats->getTrueNegativeCounter());
        self::assertEquals(0, $stats->getFalseNegativeCounter());

        self::assertEquals(1, $stats->getTruePositiveRate());
        self::assertEquals(0.9, $stats->getFalsePositiveRate());
        self::assertEquals(0.1, $stats->getTrueNegativeRate());
        self::assertEquals(0.0, $stats->getFalseNegativeRate());

        self::assertEqualsWithDelta(0.5263, $stats->getPrecision(), 0.0001);
        self::assertEquals(1, $stats->getNegativePredictionValue());
        self::assertEqualsWithDelta(0.4737, $stats->getFalseDiscoveryRate(), 0.0001);
        self::assertEquals(0, $stats->getFalseOmissionRate());
        self::assertEquals(0.55, $stats->getAccuracy());
        self::assertEqualsWithDelta(0.6897, $stats->getF1Score(), 0.0001);
    }

    public function testStatistics3()
    {
        $stats = new Statistics($this->points, 0.58);
        self::assertEquals(3, $stats->getTruePositiveCounter());
        self::assertEquals(1, $stats->getFalsePositiveCounter());
        self::assertEquals(9, $stats->getTrueNegativeCounter());
        self::assertEquals(7, $stats->getFalseNegativeCounter());

        self::assertEquals(0.3, $stats->getTruePositiveRate());
        self::assertEqualsWithDelta(0.1, $stats->getFalsePositiveRate(), 0.0001);
        self::assertEqualsWithDelta(0.9, $stats->getTrueNegativeRate(), 0.0001);
        self::assertEqualsWithDelta(0.7, $stats->getFalseNegativeRate(), 0.0001);

        self::assertEqualsWithDelta(0.75, $stats->getPrecision(), 0.0001);
        self::assertEquals(0.5625, $stats->getNegativePredictionValue());
        self::assertEqualsWithDelta(0.25, $stats->getFalseDiscoveryRate(), 0.0001);
        self::assertEquals(0.4375, $stats->getFalseOmissionRate());
        self::assertEquals(0.6, $stats->getAccuracy());
        self::assertEqualsWithDelta(0.4286, $stats->getF1Score(), 0.0001);
    }
}
