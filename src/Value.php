<?php

namespace Cylab\ROC;

/**
 * Description of Value
 *
 * @author tibo
 */
interface Value
{
    public function getScore() : float;

    public function isTrueAlert() : bool;
}
