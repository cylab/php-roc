<?php

namespace Cylab\ROC;

require __DIR__ . "/../vendor/autoload.php";

// for each value, we indicate if it is a true detection (true)
// or a false alarm (false)
$values = [];
$values[] = new SimpleValue(0.22568639331762, true);
$values[] = new SimpleValue(0.33365791865329, true);
$values[] = new SimpleValue(0.74073486204293, true);
$values[] = new SimpleValue(0.47706215198946, false);
$values[] = new SimpleValue(0.049798118439409, false);
$values[] = new SimpleValue(0.083663045933313, true);

$roc = ROC::fromValues($values);

// Get the Area Under the Curve (AUC)
echo "AUC : " . $roc->getAUC() . "\n";

// Inpect the points that draw the ROC
var_dump($roc->getPoints());

// Save ROC to png image
$roc->saveToPNG("/tmp/php-roc.png", 800, 600);
