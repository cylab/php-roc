<?php

namespace Cylab\ROC;

use PHPUnit\Framework\TestCase;

/**
 * @group roc
 *
 * To run only these tests:
 * ./vendor/bin/phpunit --group roc
 */
class ROCTest extends TestCase
{

    /**
     * @var ROC
     */
    protected $roc;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     * @throws \Exception
     */
    protected function setUp()
    {
        $this->roc = ROC::fromCSVFile(__DIR__ . "/../resources/RocSampleData.csv");
    }

    /**
     * @group all-true-detections
     */
    public function testAllTrueDetections()
    {
        $values = [];
        $values[] = new SimpleValue(0.2, true);
        $values[] = new SimpleValue(0.45, true);
        $values[] = new SimpleValue(0.57, true);
        $roc = ROC::fromValues($values);
        $this->assertEquals(1, $roc->getAUC());
        $roc->saveToPNG("/tmp/roc-all-true.png");
    }

    /**
     * @covers Cylab\ROC\ROC::computeRocPoints
     */
    public function testComputeRocPoints()
    {
        $rocCoordinatesComputed = $this->roc->getPoints();
        $coordinates = [];
        $coordinates[] = new Point(0, 0);
        $coordinates[] = new Point(0, 0.1);
        $coordinates[] = new Point(0, 0.2);
        $coordinates[] = new Point(0.1, 0.2);
        $coordinates[] = new Point(0.1, 0.3);
        $coordinates[] = new Point(0.1, 0.4);
        $coordinates[] = new Point(0.1, 0.5);
        $coordinates[] = new Point(0.2, 0.5);
        $coordinates[] = new Point(0.3, 0.5);
        $coordinates[] = new Point(0.3, 0.6);
        $coordinates[] = new Point(0.4, 0.6);
        $coordinates[] = new Point(0.4, 0.7);
        $coordinates[] = new Point(0.5, 0.7);
        $coordinates[] = new Point(0.5, 0.8);
        $coordinates[] = new Point(0.6, 0.8);
        $coordinates[] = new Point(0.7, 0.8);
        $coordinates[] = new Point(0.8, 0.8);
        $coordinates[] = new Point(0.8, 0.9);
        $coordinates[] = new Point(0.9, 0.9);
        $coordinates[] = new Point(0.9, 1);
        $coordinates[] = new Point(1, 1);
        $this->assertEquals($coordinates, $rocCoordinatesComputed);
    }

    /**
     * @covers Cylab\ROC\ROC::computeAUC
     * @todo   Implement testComputeAUC().
     */
    public function testComputeAUC()
    {
        $auc = 0.68;
        $aucComputed = $this->roc->getAUC();
        $this->assertEquals($auc, $aucComputed);
    }

    /**
     * @covers Cylab\ROC\ROC::storeRocSpaceCoordinatesToCSV
     * @todo   Implement testStoreRocSpaceCoordinatesToCSV().
     */
    public function testStoreRocSpaceCoordinatesToCSV()
    {
        $fileTest = fopen(__DIR__ . '/../resources/FileForStoreRocSpaceCoordinatesToCSVTest.csv', 'r');
        $rocPoints = $this->roc->getPoints();
        $this->roc->storeRocSpaceCoordinatesToCSV($rocPoints, __DIR__ . '/../resources/TestComparison.csv');
        $computedFile = fopen(__DIR__ . "/../resources/TestComparison.csv", 'r');
        $originalData = [];
        $computedData = [];
        while (!feof($fileTest)) {
            //$line[]  = fgetcsv($fileTest);
            $originalData[] = fgetcsv($fileTest);
        }
        while (!feof($computedFile)) {
            //$line[] = fgetcsv($computedFile);
            $computedData[] = fgetcsv($computedFile);
        }
        for ($i = 0; $i < count($computedData) - 1; $i++) {
            $this->assertEquals($originalData[$i], $computedData[$i]);
        }
    }

    /**
     * @covers Cylab\ROC\ROC::countPositiveExample
     * @throws \Exception
     * @todo   Implement testCountPositiveExample().
     */
    public function testCountPositiveExample()
    {
        $roc = ROC::fromCSVFile(__DIR__ . "/../resources/RocWowa.csv");
        $this->assertEquals(206, $roc->countTrueDetections($roc->getValues()));
    }

    /**
     * @covers Cylab\ROC\ROC::countNegativeExample
     * @todo   Implement testCountNegativeExample().
     */
    public function testCountNegativeExample()
    {
        $roc = ROC::fromCSVFile(__DIR__ . "/../resources/RocWowa.csv");
        $this->assertEquals(12261, $roc->countFalseAlarms($roc->getValues()));
    }

    /**
     * @group png
     */
    public function testSavePNG()
    {
        $roc = ROC::fromCSVFile(__DIR__ . "/../resources/RocWowa.csv");
        $roc->saveToPNG("/tmp/roc.png");
    }
}
