<?php


namespace Cylab\ROC;

use PHPUnit\Framework\TestCase;

use Amenadiel\JpGraph\Graph\Graph;
use Amenadiel\JpGraph\Plot;

/**
 * Description of JPGraphTest
 *
 * @group jpgraph
 *
 * @author tibo
 */
class JPGraphTest extends TestCase
{


    public function testBasic()
    {

        $graph = new Graph(800, 500);
        $graph->title->Set("A Simple Pie Plot");

        // Use an integer X-scale
        $graph->SetScale('intlin', 0, 1, 0, 1);
        $graph->SetBox(true);

        $datax = [0, 0.5, 0.5, 0.7, 0.7, 1];
        $datay = [0, 0,   0.5, 0.5, 1,   1];
        $p1 = new Plot\LinePlot($datay, $datax);
        $p1->SetColor('blue');

        $graph->Add($p1);

        $filename = sys_get_temp_dir() .  "/php-roc.png";
        $graph->Stroke($filename);
    }
}
