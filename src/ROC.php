<?php

/**
 * Compute a ROC curve (and AUC) from an array of scores, for which we
 * know the ground truth (we know if they are a true detection or a false
 * alarm).
 *
 * @author tibo
 * @author Alexandre
 */

namespace Cylab\ROC;

use Exception;

use Amenadiel\JpGraph\Graph\Graph;
use Amenadiel\JpGraph\Plot;
use Amenadiel\JpGraph\Text\Text;

class ROC
{
    /**
     * the initial samples, from which we build the ROC.
     * will be renamed to "values" !
     * @var array
     */
    private $values = [];

    /**
     * the points of the ROC curve, which are computed only once, when we call
     * setPoints()
     * @var array
     */
    private $points = [];

    /**
     * Value of AUC, computed only once, when we call setPoints()
     * @var float
     */
    private $auc;

    /**
     * Default constructor not used.
     * Static functions instead
     * Way to have "multiple" constructors in PHP.
     */
    private function __construct()
    {
    }

    public static function fromCSVFile(string $filename) : ROC
    {
        $file = fopen($filename, 'r');
        $values = [];
        while (!feof($file)) {
            $line = fgetcsv($file);
            if (is_numeric($line[0]) && is_numeric($line[1])) {
                $values[] = new SimpleValue(floatval($line[0]), boolval($line[1]));
            }
        }
        return self::fromValues($values);
    }

    /**
     * Static method used as a constructor. Takes arrays as argument.
     * @param array $score float array with score
     * @param array $true_alert indicates if scores are true alerts
     * @return ROC
     */
    public static function fromArrays(array $score, array $true_alert) : ROC
    {
        if (count($score) != count($true_alert)) {
            throw new Exception('True Alert and score vectors must have the same length');
        }
        $values = [];
        for ($i = 0; $i < count($score); $i++) {
            $values[] = new SimpleValue($score[$i], $true_alert[$i]);
        }
        return self::fromValues($values);
    }

    /**
     * Build a ROC from an array of values (class Value).
     *
     * @param array $values
     * @return \Cylab\ROC\ROC
     */
    public static function fromValues(array $values) : ROC
    {
        if (! is_a($values[0], Value::class)) {
            throw new Exception("Invalid class: " . get_class($values[0]));
        }

        $roc = new self();
        $roc->setValues($values);
        return $roc;
    }

    /**
     * Compute ROC curve points and AUC.
     * @return void
     */
    private function compute() : void
    {
        $total_true_detections = $this->countTrueDetections($this->values);
        $total_false_alarms = $this->countFalseAlarms($this->values);

        $true_detection = 0;
        $false_alarm = 0;

        $true_detection_previous = 0;
        $false_alarm_previous = 0;

        $area = 0;
        $points = [];
        $previous_score = -INF;

        foreach ($this->values as $value) {
            if ($value->getScore() !== $previous_score) {
                $area = $area + $this->trapezoidArea(
                    $false_alarm,
                    $false_alarm_previous,
                    $true_detection,
                    $true_detection_previous
                );

                $true_detection_ratio = $true_detection / $total_true_detections;

                if ($total_false_alarms == 0) {
                    // special case where all scores are true detections
                    // https://gitlab.cylab.be/cylab/php-roc/-/issues/6
                    $false_alarm_ratio = 0;
                } else {
                    $false_alarm_ratio = $false_alarm / $total_false_alarms;
                }

                $points[] = new Point($false_alarm_ratio, $true_detection_ratio);
                $previous_score = $value->getScore();
                $false_alarm_previous = $false_alarm;
                $true_detection_previous = $true_detection;
            }

            if ($value->isTrueAlert()) {
                $true_detection++;
            } else {
                $false_alarm++;
            }
        }

        // Last point...
        $area += $this->trapezoidArea(
            $false_alarm,
            $false_alarm_previous,
            $true_detection,
            $true_detection_previous
        );

        $true_detection_ratio = $true_detection / $total_true_detections;
        if ($total_false_alarms == 0) {
            // special case where all scores are true detections
            // https://gitlab.cylab.be/cylab/php-roc/-/issues/6
            $false_alarm_ratio = 0;
        } else {
            $false_alarm_ratio = $false_alarm / $total_false_alarms;
        }
        $points[] = new Point($false_alarm_ratio, $true_detection_ratio);

        $this->points = $points;

        if ($total_false_alarms == 0) {
            // special case where all scores are true detections
            // https://gitlab.cylab.be/cylab/php-roc/-/issues/6
            $this->auc = 1;
        } else {
            $this->auc = $area / ($total_true_detections * $total_false_alarms);
        }
    }

    /**
     * Compute trapezoid area (intermediate area for AUC)
     * @param float $x1
     * @param float $x2
     * @param float $y1
     * @param float $y2
     * @return float
     */
    private function trapezoidArea(float $x1, float $x2, float $y1, float $y2) : float
    {
        $base = abs($x1 - $x2);
        $heightAverage = ($y1 + $y2) / 2;
        return ($base * $heightAverage);
    }

    /**
     * Store RocSpaceCoordinates in a csv file
     * @param array $RocSpaceCoordinates. Provided by computeRocPoints method
     * @param string $fileName File path to store RocSpaceCoordinates
     */
    public function storeRocSpaceCoordinatesToCSV(array $RocSpaceCoordinates, string $fileName)
    {
        $file = fopen($fileName, "w");
        fputcsv($file, array('False Alarm', 'True Detection'));
        foreach ($RocSpaceCoordinates as $coordinate) {
            fputcsv($file, array($coordinate->getX(), $coordinate->getY()));
        }
        return $file;
    }
    /**
     * Count the number of negative elements in the set of data
     * @param array $points
     * @return int
     */
    public function countFalseAlarms(array $points)
    {
        $counter = 0;
        foreach ($points as $point) {
            if (! $point->isTrueAlert()) {
                $counter++;
            }
        }
        return $counter;
    }
    /**
     * Count the number of positive elements in the set of data
     * @param array $points
     * @return int
     */
    public function countTrueDetections(array $points)
    {
        $counter = 0;
        foreach ($points as $point) {
            if ($point->isTrueAlert()) {
                $counter++;
            }
        }
        return $counter;
    }

    /**
     * Setter for $points
     * @param array $values
     */
    public function setValues(array $values)
    {
        $this->values = $values;
        self::sort($this->values);
        $this->compute();
    }

    /**
     * Getter for values
     * @return Value[]
     */
    public function getValues()
    {
        return $this->values;
    }

    /**
     * Get the Points of the computed ROC.
     * @return array
     */
    public function getPoints() : array
    {
        return $this->points;
    }

    public function getAUC() : float
    {
        return $this->auc;
    }

    /**
     * Get the values of the x axis (false alarm).
     * @return array
     */
    public function getX() : array
    {
        $x = [];
        foreach ($this->points as $point) {
            $x[] = $point->getX();
        }
        return $x;
    }

    /**
     * Get the values of the x axis (false alarm).
     * @return array
     */
    public function getY() : array
    {
        $y = [];
        foreach ($this->points as $point) {
            $y[] = $point->getY();
        }
        return $y;
    }

    public function saveToPNG(string $filename, int $width = 1200, int $height = 800)
    {
        $graph = new Graph($width, $height);
        $graph->title->Set("Receiver Operating Characteristic (ROC)");
        $graph->title->SetMargin(15);

        // Use an integer X-scale
        $graph->SetScale('lin', 0, 1, 0, 1);
        $graph->SetBox(true);

        $p1 = new Plot\LinePlot($this->getY(), $this->getX());
        $p1->SetColor('blue');

        $graph->Add($p1);

        $graph->xaxis->setTitle("false alarm rate", "center");
        $graph->xaxis->setTitleMargin(15);
        $graph->xaxis->setTickPositions(range(0, 1, 0.1), []);

        $graph->yaxis->setTitle("true detection rate", "middle");
        $graph->yaxis->setTitleMargin(30);
        $graph->yaxis->setTickPositions(range(0, 1, 0.1), []);

        $txt = new Text('AUC: ' . $this->getAUC());

        // Position the string at absolute pixels (0,20).
        // ( (0,0) is the upper left corner )
        $txt->SetPos(0.95, 0.92, 'right', 'bottom');//, 'right', 'bottom');

        // Set color and fonr for the text
        //$txt->SetColor('red');
        //$txt->SetFont(FF_FONT2, FS_BOLD);

        // ... and add the text to the graph
        $graph->AddText($txt);


        // left, right, top, bottom
        $graph->SetMargin(50, 50, 50, 40);

        $graph->Stroke($filename);
    }

    /**
     * Function to store values array by decreasing score
     * @param array $points
     * @return bool
     */
    public static function sort(array &$points)
    {
        return usort($points, function (Value $a, Value $b) {
            if ($a->getScore() < $b->getScore()) {
                return +1;
            } elseif ($a->getScore() > $b->getScore()) {
                return -1;
            } else {
                return 0;
            }
        });
    }
}
