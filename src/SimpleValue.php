<?php

namespace Cylab\ROC;

use Exception;

/**
 * Store a score in range [0 .. 1] and indicate if it is a true alert or false
 * alarm.
 *
 * @author alex
 */
class SimpleValue implements Value
{
    private $score;
    private $true_alert;

    /**
     * Create a value consisting of a score (0 <= score <= 1) and the
     * indication if it is a true alert (true) or a false alarm (false).
     *
     * @param float $score
     * @param bool $true_alert
     * @throws Exception
     */
    public function __construct(float $score, bool $true_alert)
    {
        $this->setScore($score);
        $this->setTrueAlert($true_alert);
    }

    /**
     * score setter. Must be between 0 and 1
     * @param float $score
     * @throws Exception
     */
    public function setScore(float $score)
    {
        if ($score > 1.0) {
            if ($score < 1.000000000000001) {
                $score = 1;
            } else {
                throw new Exception("Score must be positive and lower or equal to 1");
            }
        } elseif ($score < 0) {
            if ($score > -0.000000000000001) {
                $score = 0;
            } else {
                throw new Exception("Score must be positive and lower or equal to 1");
            }
        }
        $this->score = $score;
    }

    /**
     * true_ alert setter
     * @param bool $true_alert
     */
    public function setTrueAlert(bool $true_alert) : void
    {
        $this->true_alert = $true_alert;
    }

    /**
     * score getter
     * @return float $score
     */
    public function getScore() : float
    {
        return $this->score;
    }


    public function isTrueAlert() : bool
    {
        return $this->true_alert;
    }

    public function isFalseAlarm() : bool
    {
        return !$this->true_alert;
    }
}
