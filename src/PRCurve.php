<?php


namespace Cylab\ROC;

class PRCurve
{

    private $points = [];

    /**
     * Default constructor not used.
     * Static functions instead
     * Way to have "multiple" constructors in PHP.
     * PRCurve constructor.
     */
    public function __construct()
    {
    }

    /**
     * Static method used as a constructor. take arrays as argument.
     * @param array $score
     * @param array $true_alert
     * @return PRCurve
     * @throws \Exception
     */
    public static function byArray(array $score, array $true_alert) : PRCurve
    {
        $instance = new self();
        $points = [];
        if (count($score) != count($true_alert)) {
            throw new \Exception("Score array and true alert array must have the same size");
        }
        for ($i = 0; $i < count($score); $i++) {
            try {
                $points[] = new SimpleValue($score[$i], $true_alert[$i]);
            } catch (\Exception $e) {
            }
        }
        $instance->setPoints($points);
        return $instance;
    }

    /**
     * Static method used as a constructor. Take a filename as argument
     * @param mixed $filename
     * @return PRCurve
     */
    public static function byFile($filename, $delimiter = ",") : PRCurve
    {
        $instance = new self();
        $file = fopen($filename, 'r');
        $points = [];
        while (!feof($file)) {
            $line = fgetcsv($file, 0, $delimiter);
            // a blank line
            if ($line == false) {
                continue;
            }
            $points[] = new SimpleValue(floatval($line[0]), boolval(floatval($line[1])));
        }
        $instance->setPoints($points);
        return $instance;
    }

    private function setPoints(array $points)
    {
        $this->points = $points;
    }

    /**
     * Method to compute points to plot the P-R curve.
     * Based on NEED TO CHECK PAPER !!!
     * @return array
     * @throws \Exception
     */
    public function computePrcPoints() : array
    {
        $iteration = 0.001;
        $threshold = 1.0;
        $curve_points = []; //CurveCoordinates list
        $actual_stat = new Statistics($this->points, $threshold);
        $next_stat = new Statistics($this->points, $threshold - $iteration);
        while (is_nan($actual_stat->getPrecision())
            && is_nan($next_stat->getPrecision())) {
            $threshold = $threshold - $iteration;
            $actual_stat = new Statistics($this->points, $threshold);
            $next_stat = new Statistics($this->points, $threshold - $iteration);
        }
        if (is_nan($actual_stat->getPrecision())) {
            if ($next_stat->getTruePositiveCounter() == 0) {
                $point = new Point($next_stat->getRecall(), $next_stat->getPrecision());
                $curve_points[] = $point;
            } else {
                $actual_stat->setPrecision($next_stat->getPrecision());
                $p1 = new Point(
                    $actual_stat->getRecall(),
                    $actual_stat->getPrecision()
                );
                $p2 = new Point(
                    $next_stat->getRecall(),
                    $next_stat->getPrecision()
                );
                $curve_points[] = $p1;
                $curve_points[] = $p2;
            }
        }
        for ($i = $threshold - $iteration; $i > 0.0; $i -= $iteration) {
            $actual_stat = new Statistics($this->points, $i);
            $next_stat = new Statistics($this->points, $i - $iteration);
            $inter = Utils::interpolationPoint($actual_stat, $next_stat);
            $a = new Point($actual_stat->getRecall(), $actual_stat->getPrecision());
            $n = new Point($next_stat->getRecall(), $next_stat->getPrecision());
            $curve_points[] = $a;
            $curve_points[] = $inter;
            $curve_points[] = $n;
        }
        return $curve_points;
    }

    /**
     * Method to compute AUC of a P-R curve
     * @return float
     * @throws \Exception
     */
    public function computePRAUC() : float
    {
        $pr_coordinates = $this->computePrcPoints();
        $previous = null;
        $actual = null;
        $area = 0;
        for ($i = 1; $i < count($pr_coordinates) - 1; $i++) {
            $previous = $pr_coordinates[$i - 1];
            $actual = $pr_coordinates[$i];
            $area = $area + $this->trapezoidArea($previous, $actual);
        }
        return $area;
    }

    /**
     * Compute trapezoid area (intermediate area for AUC)
     * @param Point $previous
     * @param Point $actual
     * @return float
     */
    private function trapezoidArea(Point $previous, Point $actual) : float
    {
        $b1 = $previous->getY();
        $b2 = $actual->getY();
        $height = abs($actual->getX() - $previous->getX());
        return ((($b1 + $b2) * $height) / 2);
    }
}
