<?php

namespace Cylab\ROC;

use PHPUnit\Framework\TestCase;

/**
 * @group prcurve
 */
class PRCurveTest extends TestCase
{

    /**
     * @group boolval
     */
    public function testBoolVal()
    {
        $this->assertEquals(false, boolval(0.0));
        $this->assertEquals(false, boolval(0));
        // "0.0" is NOT evaluated to false!
        $this->assertEquals(true, boolval("0.0"));
        // but it IS false, if we first convert to float...
        $this->assertEquals(false, boolval(floatval("0.0")));
        $this->assertEquals(false, boolval("0"));

        $this->assertEquals(true, boolval("1.0"));
    }

    public function testComputePRAUC()
    {
        $filename = __DIR__ . "/../resources/CSC.csv";
        $pr = PRCurve::byFile($filename);
        $auc = $pr->computePRAUC();
        self::assertEqualsWithDelta(0.5074738, $auc, 0.0001);
    }
}
