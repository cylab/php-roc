# php-roc
[![pipeline status](https://gitlab.cylab.be/cylab/php-roc/badges/master/pipeline.svg)](https://gitlab.cylab.be/cylab/php-roc/commits/master)
[![coverage report](https://gitlab.cylab.be/cylab/php-roc/badges/master/coverage.svg)](https://gitlab.cylab.be/cylab/php-roc/-/commits/master)
[![Packagist Version](https://img.shields.io/packagist/v/cylab/php-roc.svg)](https://packagist.org/packages/cylab/php-roc)

PHP library for computing Receiver Operating Characteristic (ROC) and Area Under the Curve (AUC).

![source: https://en.wikipedia.org/wiki/Receiver_operating_characteristic](roc.svg)

A Receiver Operating Characteristic curve (ROC curve) is a a graphical plot that illustrates the diagnostic ability of a binary classifier system  when its discrimination threshold is varied.
The ROC curve is plotted with the False Positive Rate (False alarm) on the X axis and the True Positive Rate (true detection) on the Y axis. It is a very interesting tool to determine the efficiency of a classifier in machine learning.
The Area Under the Curve (AUC) is a simple way to compare different ROC curves and to determine the best one. Closer to 1 is the AUC value, better is the classifier. 

## Installation

```bash
composer require cylab/php-roc
```

## Usage

### Array of values

```php
namespace Cylab\ROC;

require __DIR__ . "/../vendor/autoload.php";

// for each value, we indicate if it is a true detection (true)
// or a false alarm (false)
$values = [];
$values[] = new SimpleValue(0.22568639331762, true);
$values[] = new SimpleValue(0.33365791865329, true);
$values[] = new SimpleValue(0.74073486204293, true);
$values[] = new SimpleValue(0.47706215198946, false);
$values[] = new SimpleValue(0.049798118439409, false);
$values[] = new SimpleValue(0.083663045933313, true);

$roc = ROC::fromValues($values);

// Get the Area Under the Curve (AUC)
echo "AUC : " . $roc->getAUC() . "\n";

// Inpect the points that draw the ROC
var_dump($roc->getPoints());

// Save ROC to png image
$roc->saveToPNG("/tmp/php-roc.png", 800, 600);
```

The ROC is built from an array of ```Value``` objects. A ```Value``` contains a score (between 0 and 1) and indicates if this is a true alert (true) or false alarm (false). 

Once built, you can get:

* the Area Under the Curve : ```getAUC()```
* the coordinates of the points that draw the ROC: ```getPoints()```
* save the ROC as a png image: ```saveToPNG($filename, $width, $height)```

![php-roc](php-roc.png)
