<?php


namespace Cylab\ROC;

/**
 * Class Utils
 * Some utilities used in ROC and P-R curves computation.
 * @package Cylab\ROC
 */
class Utils
{

    /**
     * Method that interpolates a point (P-R curve) between 2 another (P-R curve points).
     * CHECK PAPER
     * @param Statistics $actual
     * @param Statistics $next
     * @return Point
     * @throws \Exception
     */
    public static function interpolationPoint(Statistics $actual, Statistics $next) : Point
    {
        $interp_recall = ($next->getRecall() - $actual->getRecall()) / 2;
        $x = (abs(($next->getTruePositiveCounter() - $actual->getTruePositiveCounter()))) / 2;
        if ($next->getTruePositiveCounter() - $actual->getTruePositiveCounter() == 0) {
            $denom_denom = 0.0;
        } else {
            $denom_denom = (($next->getFalsePositiveCounter() - $actual->getFalsePositiveCounter()) /
                    ($next->getTruePositiveCounter() - $actual->getTruePositiveCounter())) * $x;
        }
        $denom = $actual->getTruePositiveCounter() + $x +
            $actual->getFalsePositiveCounter() + $denom_denom;
        $interp_prec = ($actual->getTruePositiveCounter() + $x) / $denom;
        return new Point(
            $actual->getRecall() + $interp_recall,
            $interp_prec
        );
    }
}
