<?php
namespace Cylab\ROC;

use Exception;

/**
 * Represents a Point (x, y), where x and y must be in range [0 .. 1].
 *
 * Use in the ROC to represent true detection (x) versus false alarm (y).
 *
 * @author alex
 */
class Point
{
    private float $x;
    private float $y;

    /**
     * Constructor for CurveCoordinates
     * @param float $x
     * @param float $y
     * @throws Exception
     */
    public function __construct(float $x, float $y)
    {
        $this->setX($x);
        $this->setY($y);
    }

    /**
     * falseAlarm getter
     * @return float $falseAlarm
     */
    public function getX() : float
    {
        return $this->x;
    }

    /**
     * trueDetection getter
     * @return float $trueDetection
     */
    public function getY() : float
    {
        return $this->y;
    }

    /**
     * falseAlarm setter. Must be between 0 and 1
     * @param float $x
     * @throws Exception
     */
    public function setX(float $x)
    {
        if ($x < 0.0 || $x > 1.0) {
            throw new \Exception('False alarm value must be between 0 and 1');
        }
        $this->x = $x;
    }
    /**
     * trueDetection setter? Must be between 0 and 1
     * @param float $y
     * @throws Exception
     */
    public function setY(float $y)
    {
        if ($y < 0 || $y > 1) {
            throw new \Exception('True detection value must be between 0 and 1');
        }
        $this->y = $y;
    }
}
