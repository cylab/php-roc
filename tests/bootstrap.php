<?php
/**
 * Bootstrap file
 *
 * @file     Bootstrap
 * @category None
 * @package  Tests
 * @author   Alexandre Croix <croix.alexandre@gmail.com>
 */
require_once __DIR__ . "/../vendor/autoload.php";
